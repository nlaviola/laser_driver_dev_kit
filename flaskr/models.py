"""Data models."""
import time
from . import db, ma
from marshmallow import Schema, fields, validates_schema, ValidationError

class Settings(db.Model):
  """Data model for device settings"""
  __tablename__ = 'settings'
  id = db.Column(db.Integer, primary_key=True)
  laser_status = db.Column(db.Integer)
  ltc_active_laser = db.Column(db.String(32))

  ltc_evcsel_to_pkg_span = db.Column(db.Float)
  ltc_evcsel_to_pkg_current = db.Column(db.Float)

  ltc_evcsel_butterfly_laser_span = db.Column(db.Float)
  ltc_evcsel_butterfly_laser_current = db.Column(db.Float)

  ltc_evcsel_butterfly_boa_span = db.Column(db.Float)
  ltc_evcsel_butterfly_boa_current = db.Column(db.Float)

  ltc_pump_span = db.Column(db.Float)
  ltc_pump_current = db.Column(db.Float)
  
  ltc_max_current = db.Column(db.Float)
  tuning_type = db.Column(db.String(32))
  vs_mems_voltage = db.Column(db.Integer)
  vs_amplifier_gain = db.Column(db.Integer)
  tw_sample_rate = db.Column(db.Integer)
  tw_voltage_step = db.Column(db.Integer)
  tw_max_voltage = db.Column(db.Integer)
  tw_min_voltage = db.Column(db.Integer)
  tec_temperature = db.Column(db.Float)
  updated = db.Column(db.Integer, onupdate=int(time.time()))

class SettingsSchema(ma.SQLAlchemyAutoSchema):
  class Meta:
    model = Settings

