import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate

db = SQLAlchemy()
ma = Marshmallow()
migrate = Migrate()

def create_app(test_config=None):
  # create and configure the app
  app = Flask(__name__, instance_relative_config=True)
  app.config.from_mapping(
    SECRET_KEY='dev',
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(app.instance_path, 'laserpitaya.db'),
    SQLALCHEMY_ECHO = False,
    SQLALCHEMY_TRACK_MODIFICATIONS = False,
    MQTT_HOST = 'localhost' #used for testing but should be overwritten in config.py instance folder
  )

  if test_config is None:
    # load the instance config, if it exists, when not testing
    app.config.from_pyfile('config.py', silent=True)
  else:
    # load the test config if passed in
    app.config.from_mapping(test_config)

  # ensure the instance folder exists
  try:
    os.makedirs(app.instance_path)
  except OSError:
    pass

  db.init_app(app)
  ma.init_app(app)
  migrate.init_app(app, db, render_as_batch=True)

  with app.app_context():
    from .models import Settings
    db.create_all()

    #import blueprints
    from . import landing, manual, settings, static_tuning
    app.register_blueprint(landing.bp)  
    app.register_blueprint(manual.bp)
    app.register_blueprint(settings.bp)
    app.register_blueprint(static_tuning.bp)  
  
    return app
