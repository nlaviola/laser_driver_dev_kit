from flask import (
    Blueprint, current_app, flash, g, jsonify, redirect, render_template, request, send_from_directory, url_for
)
import os
from werkzeug.exceptions import abort

bp = Blueprint('manual', __name__, url_prefix='/manual')

@bp.route('/')
def index():
  return render_template('manual.html')