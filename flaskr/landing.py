from flask import (
    Blueprint, current_app, flash, g, jsonify, redirect, render_template, request, send_from_directory, url_for
)
import os
from werkzeug.exceptions import abort

bp = Blueprint('landing', __name__)

@bp.route('/')
def index():
  return render_template('landing.html')