from typing import Dict
from flask import (
    Blueprint, current_app, flash, g, jsonify, redirect, render_template, request, send_from_directory, url_for
)
import os, json
from werkzeug.exceptions import abort

from marshmallow.exceptions import ValidationError
from sqlalchemy import exc

#Paho-MQTT setup
from paho.mqtt.client import Client
c = Client()

from .models import db, Settings, SettingsSchema

bp = Blueprint('static_tuning', __name__, url_prefix='/static_tuning')

@bp.route('/')
def index():
  # query for settings data
  settings_data = Settings.query.get(1)

  print(settings_data)

  return render_template('applications/static_tuning.html', settings = settings_data)

@bp.route('/update_laser_status', methods=['PUT'])
def update_laser_status():
  #get updated laser status
  updated_laser_status = int(request.form['laser_status'])

  # query for laser settings
  settings_data = Settings.query.get(1)

  #update settings
  settings_data.laser_status = updated_laser_status
  try:
    db.session.commit()
    #flash('laser updated!')
  except exc.SQLAlchemyError as e:
    db.session.rollback()
    #set flash data
    flash('DB error, contact a web administrator', 'error')
    return jsonify({"error": "db insertion error", "code": 500})

  laser_parameters = SettingsSchema().dump(settings_data)

  #get the MQTT broker from config.py
  mqtt_host = current_app.config['MQTT_HOST']

  # turn laser OFF
  if updated_laser_status == 0:

    laser_parameters['status'] = 'OFF'

    #publish laser OFF to MQTT
    c.connect(mqtt_host)
    c.publish("laser/status", json.dumps(laser_parameters))
    c.loop()
    c.disconnect()

    # flash data
    flash('Laser is now OFF!', 'success')
    return jsonify({"laser_status": "OFF", "code": 200})

  # turn laser ON
  if updated_laser_status == 1:

    laser_parameters['status'] = 'ON'

    # validate currents against laser types in python script
    # warn user if out of spec and give option to enter new values
    # set up channel data
    active_laser = settings_data.ltc_active_laser
    if active_laser == 'pump':
      if(float(settings_data.ltc_pump_current) > 250):
        return jsonify({"laser_status": "ERROR", 'msg': 'Pump current must be at or below 250mA', "code": 400})

    if active_laser == 'to_can_evcsel':
      if(float(settings_data.ltc_evcsel_to_pkg_current) > 10):
        return jsonify({"laser_status": "ERROR", 'msg': 'To Can PKG Laser current must be at or below 10mA', "code": 400})

    if active_laser == 'butterfly':
      if(float(settings_data.ltc_evcsel_butterfly_boa_current) > 200 or float(settings_data.ltc_evcsel_butterfly_laser_current) > 10 ):
        return jsonify({"laser_status": "ERROR", 'msg': 'Butterfly BOA current must be at or below 200mA. Butterfly Laser Current must be at or below 10mA', "code": 400})

    
    #publish laser ON and parameters to MQTT
    c.connect(mqtt_host)
    c.publish("laser/status", json.dumps(laser_parameters))
    c.loop()
    c.disconnect()

    # flash data for page reload
    flash('Laser is now ON!', 'success')

    return jsonify({"laser_status": "ON", "code": 200})

@bp.route('/update_tuning_type', methods=['PUT'])
def update_tuning_type():
  if request.method == 'PUT':
    # get form data keys
    updated_tuning_type = request.form['tuning_type']
    # query for laser settings
    settings_data = Settings.query.get(1)

    settings_data.tuning_type = updated_tuning_type

    # commit update
    try:
      db.session.commit()
    except exc.SQLAlchemyError as e:
      db.session.rollback()
      return {"error": "Problem updating paramaters, please contact the admin"}, 400

    flash('Tuning type updated!', 'success')

    return jsonify({"id": settings_data.id, "code": 204})