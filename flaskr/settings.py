from flask import (
    Blueprint, current_app, flash, g, jsonify, redirect, render_template, request, send_from_directory, url_for
)
import os
from werkzeug.exceptions import abort

from marshmallow.exceptions import ValidationError
from sqlalchemy import exc

from .models import db, Settings, SettingsSchema

bp = Blueprint('settings', __name__, url_prefix='/settings')

@bp.route('/')
def index():
  return render_template('settings.html')

@bp.route('/get_settings')
def get_settings():
  if request.method == 'GET':
    settings_data = Settings.query.get(1)
    
    output = jsonify(SettingsSchema().dump(settings_data))

    return output, 200

@bp.route('/update_active_laser', methods=['PUT'])
def update_active_laser():
  if request.method == 'PUT':
    updated_active_laser = request.form['ltc_active_laser']
    
    settings_data = Settings.query.get(1)

    settings_data.ltc_active_laser = updated_active_laser

    try:
      db.session.commit()
      flash('laser updated!')
    except exc.SQLAlchemyError as e:
      db.session.rollback()
      return {"error": "Problem inserting Oxcal Data, please contact the admin"}, 400

    return jsonify({"id": settings_data.id, "code": 204})

@bp.route('/update_settings', methods=['PUT'])
def update_settings():
  if request.method == 'PUT':
    # get form data keys
    formDataKeys = request.form.keys()
    # query for laser settings
    settings_data = Settings.query.get(1)

    #loop through form data keys and update appropriate fields 
    for key in formDataKeys:
      setattr(settings_data, key, request.form[key])

    # commit update
    try:
      db.session.commit()
    except exc.SQLAlchemyError as e:
      db.session.rollback()
      return {"error": "Problem updating paramaters, please contact the admin"}, 400

    return jsonify({"id": settings_data.id, "code": 204})
