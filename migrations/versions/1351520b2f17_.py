"""empty message

Revision ID: 1351520b2f17
Revises: 3ff0b058e467
Create Date: 2021-11-16 14:59:36.488668

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1351520b2f17'
down_revision = '3ff0b058e467'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('settings', schema=None) as batch_op:
        batch_op.add_column(sa.Column('tuning_type', sa.String(length=32), nullable=True))
        batch_op.add_column(sa.Column('tw_sample_rate', sa.Integer(), nullable=True))
        batch_op.add_column(sa.Column('tw_max_voltage', sa.Integer(), nullable=True))
        batch_op.add_column(sa.Column('tw_min_voltage', sa.Integer(), nullable=True))

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('settings', schema=None) as batch_op:
        batch_op.drop_column('tw_min_voltage')
        batch_op.drop_column('tw_max_voltage')
        batch_op.drop_column('tw_sample_rate')
        batch_op.drop_column('tuning_type')

    # ### end Alembic commands ###
